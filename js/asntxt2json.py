#!/usr/bin/python

import json
import re

with open("asn2.txt") as f:
    AStext = f.readlines()

ASnames = {}

for line in AStext:
    # Ignore the header lines
    if not re.match('\s*\d+',line):
        continue
    fields = line.split()
    ASnames[fields[0]] = fields[1]

print "Loaded %d AS names" % len(ASnames)

with open("asn.json","w") as f:
    f.write("var ASNames = ")
    json.dump(ASnames,f)
    f.write(";")

